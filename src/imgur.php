<?php

class Imgur
{
	private $config;
	
	public function __construct()
	{
		$this->config = include dirname(__FILE__) . '/../cfg/config.php';
	}
	/**
	 * @param string $key
	 * return mixed
	*/
	public function getConfig($key = null)
	{
		return !empty($key) ? $this->config[$key] : $this->config;
	}
	/**
	 * @return array 
	*/
	public function getImages() 
	{
		$processedImages = 0;
		$imagesPerPage   = 10;
		$howmany         = 1;	// This is a lie; imgur returns 60 on a single iteration. So make 1; because what else can we do?
		
		$results         = [];
		
		$curl = curl_init();
		
		for ($i = 0; $i < $howmany; $i++) {
			
			curl_setopt($curl, CURLOPT_HTTPHEADER, ['Authorization: Client-ID ' . $this->getConfig('client_id')]); 
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_URL, $this->getConfig('api_endpoint') . 'gallery/random/random/' . $i . '.json');
		
			foreach (json_decode(curl_exec($curl))->data as $key => $result) {

				if ($key === 'error') {
					
					$results[0] = $result;
					break 2;
					
				} elseif ($processedImages >= $images) { 
					
					$results[] = $result;
					$processedImages++;
				}
			}
		
			if ($i >= $pages) {
				$pages++;
			}
		}
		curl_close($curl);
		return $results;
	}
}