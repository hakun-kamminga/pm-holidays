<?php
namespace imgur;
use Imgur;
/**
 * This class provides the http interactions
 * between the application and imgur.
*/ 

class http
{
	/** @var imgur\Imgur $imgur **/
	protected $imgur;
	public function __construct(
		Imgur $imgur
	)
	{
		$this->imgur = $imgur;
	}
		
}